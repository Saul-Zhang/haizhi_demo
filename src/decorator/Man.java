package decorator;

/**
 * Created by Saul on 2019/6/18 17:53
 * 具体组件：将要被附加功能的类，实现抽象构件角色接口
 */
public class Man implements Person {
    @Override
    public void eat() {
        System.out.println("男人在吃");
    }
}
