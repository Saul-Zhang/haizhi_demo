package decorator;

/**
 * Created by Saul on 2019/6/18 17:55
 * 抽象装饰者：持有对具体构件角色的引用并定义与抽象构件角色一致的接口
 */
public abstract class Decorator implements Person {
    private  Person person;

    public void setPerson(Person person) {
        this.person = person;
    }

    @Override
    public void eat() {
        person.eat();
    }
}
