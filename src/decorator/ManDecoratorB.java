package decorator;

/**
 * Created by Saul on 2019/6/18 18:03
 * @author Saul
 */
public class ManDecoratorB extends Decorator {
    @Override
    public void eat() {
        super.eat();
        System.out.println("---------");
        System.out.println("ManDecoratorB类");
    }
}
