package decorator;

/**
 * Created by Saul on 2019/6/18 17:52
 * 抽象组件:定义一个抽象接口，来规范准备附加功能的类
 */
public interface Person {
    void eat();
}
