package decorator;

/**
 * Created by Saul on 2019/6/18 18:08
 */
public class Test {
    public static void main(String[] args) {
        Man man = new Man();
        ManDecoratorA manDecoratorA = new ManDecoratorA();
        ManDecoratorB manDecoratorB = new ManDecoratorB();
        manDecoratorA.setPerson(man);
//        manDecoratorA.eat();
        manDecoratorB.setPerson(manDecoratorA);
        manDecoratorB.eat();
    }
}
