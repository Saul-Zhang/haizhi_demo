package decorator;

/**
 * Created by Saul on 2019/6/18 18:00
 * 具体装饰：实现抽象装饰者角色，负责对具体构件添加额外功能
 */
public class ManDecoratorA extends Decorator {

    @Override
    public void eat() {
        super.eat();
        reEat();
        System.out.println("ManDecoratorA类");
    }

    public void reEat(){
        System.out.println("eat again");
    }
}
