package model;

import myORM.core.Column;
import myORM.core.Id;
import myORM.core.Table;

import java.util.Date;

/**
 * Created by Saul on 2019/6/19 9:47
 */
@Table(name = "student")
public class Student {
    @Id(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "age",type = "int")
    private Integer age;

    @Column(name = "birthday",type ="Date")
    private Date birthday;

    public Student() {
    }

    public Student(Integer id, String name, Integer age, Date birthday) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.birthday = birthday;
    }

    public Student(Integer id, String name, Integer age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
}
