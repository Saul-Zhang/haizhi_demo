package model;

import org.w3c.dom.NodeList;

/**
 * Created by Saul on 2019/6/18 11:21
 */
public class Book {
    private Integer id;
    private String name;
    private String author;
    private Integer year;

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    private Double price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Book{" +
                "nama='" + name + '\'' +
                ", author='" + author + '\'' +
                ", year=" + year +
                ", privice=" + price +
                '}';
    }
}
