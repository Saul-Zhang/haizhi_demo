package readXML;

import model.Book;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.util.ArrayList;
import java.util.List;

/**
 * 通过SAX的方式解析XML
 * Created by Saul on 2019/6/18 11:53
 */
public class ReadXMLBySAX {
    private static List<Book> books = null;

    private SAXParserFactory sParserFactory = null;
    private SAXParser parser = null;


    public List<Book> getBooks(String fileName) throws Exception {
        sParserFactory = SAXParserFactory.newInstance();
       parser = sParserFactory.newSAXParser();
        SAXParseHandler handler = new SAXParseHandler();
        parser.parse(fileName, handler);
        return handler.getBooks();
    }
    public static void main(String[] args) {
        try {
            books = new ReadXMLBySAX().getBooks("src/res/book.xml");
            for(Book book:books){
                System.out.println(book);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}

class SAXParseHandler extends DefaultHandler {
    /**
     * 存放解析到的book数组
     */
    private List<Book> list;
    /**
     * 存放当前解析的book
     */
    private Book book;
    /**
     * 存放当前节点值
     */
    private String content = null;

    @Override
    public void startDocument() throws SAXException {
        super.startDocument();
        System.out.println("开始解析xml文件");
        list = new ArrayList<>();
    }

    @Override
    public void endDocument() throws SAXException {
        super.endDocument();
        System.out.println("xml文件解析完毕");
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, qName, attributes);
        System.out.println("uri----"+uri);
        System.out.println("localName----"+localName);
        //当节点名为book时,获取book的属性id
        if ("book".equals(qName)) {
            book = new Book();
            String id = attributes.getValue("id");
            //System.out.println("id值为"+id);
            book.setId(Integer.parseInt(id));
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);
        if (("name").equals(qName)) {
            book.setName(content);
        } else if (("author").equals(qName)) {
            book.setAuthor(content);
        } else if (("year").equals(qName)) {
            book.setYear(Integer.parseInt(content));
        } else if (("price").equals(qName)) {
            book.setPrice(Double.parseDouble(content));
        } else if (("book").equals(qName)) {
            //当结束当前book解析时,将该book添加到数组后置为空，方便下一次book赋值
            list.add(book);
            book = null;
        }

    }

    /**
     * 获取节点的值
     *
     * @param ch
     * @param start
     * @param length
     * @throws SAXException
     */
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        super.characters(ch, start, length);
        content = new String(ch, start, length);
        if (!content.trim().equals("")) {
            System.out.println("节点值为：" + content);
        }

    }
    public List<Book> getBooks(){
        return list;
    }
}

