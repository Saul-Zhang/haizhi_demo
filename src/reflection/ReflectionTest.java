package reflection;

import model.Book;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by Saul on 2019/6/18 15:48
 */
public class ReflectionTest {
    public static void main(String[] args) {
        Class clazz = Book.class;
        /**
         * 获取当前类及父类非private方法
         */
        Method[] methods = clazz.getMethods();
        for (Method method : methods) {
            System.out.print(" " + method.getName());
        }
        System.out.println();

        /**
         * 获取当前类的所有方法
         */
        Method[] declaredMethods = clazz.getDeclaredMethods();
        for (Method declaredMethod : declaredMethods) {
            System.out.print(" " + declaredMethod.getName());
        }
        System.out.println();
        /**
         * 获取指定方法
         */
        Method method = null;
        try {
            method = clazz.getDeclaredMethod("setName", String.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        System.out.println(method);

        /**
         * 执行方法
         */
        Object obje = null;
        try {
            obje = clazz.newInstance();
            method.invoke(obje, "xxxx");
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
