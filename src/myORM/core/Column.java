package myORM.core;

import java.lang.annotation.*;

/**
 * Created by Saul on 2019/6/19 10:09
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Column {
    String name();

    String type() default "string";

    int length() default 20;
}
