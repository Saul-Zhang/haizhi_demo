package myORM.core;

import java.lang.annotation.*;

/**
 * Created by Saul on 2019/6/19 9:51
 */
// ElementType.Type : 作用于接口，类，枚举，注解
@Target(ElementType.TYPE)
//注解不仅被保存到class文件中，jvm加载class文件之后，仍然存在
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Table {
    String name();
}
