package myORM.core;

import java.lang.annotation.*;

/**
 * Created by Saul on 2019/6/19 10:04
 * 用在id上的注解
 */
// 用在字段上
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Id {
    String name();
    String type() default "int";
    int length() default 20;
    int increment() default 1;
}
