package myORM.utils;

import java.sql.*;

/**
 * Created by Saul on 2019/6/19 10:17
 */
public class JdbcUtils {

    public static Connection getConn(){
        Connection conn =null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql:///my_orm", "root", "123456");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("获取连接对象失败.");
        }
        return conn;
    }

    /**
     * 增删改操作
     * @param sql sql语句
     * @param params 参数
     * @return 结果
     */
    public static int excuteUpdate(String sql, Object[] params){
        Connection connection = null;
        PreparedStatement pstmt =null;
        int result = -1;
        try {
            connection = getConn();
            pstmt = connection.prepareStatement(sql);
            for (int i = 0; i < params.length; i ++){
                pstmt.setObject(i+1, params[i]);
        }
            result = pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println("更新数据出现异常.");
            System.out.println(e.getMessage());
        } finally {
            release(pstmt, connection);
        }
        // 更新数据失败
        return result;
    }
    public static void release(Statement stmt, Connection conn) {
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            stmt = null;
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            conn = null;
        }
    }

    public static void main(String[] args) {
        getConn();
    }
}
