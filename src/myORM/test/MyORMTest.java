package myORM.test;

import model.Student;
import myORM.executes.Operation;

/**
 * Created by Saul on 2019/6/19 11:12
 */
public class MyORMTest {
    static Operation<Student> operation = new Operation<>();
    public static void main(String[] args) {
//        Student student = new Student(1, "zzz", 30);
//        operation.add(student);

        Student student1 = new Student(1, "zzz11", 30);
        operation.update(student1);
    }
}
