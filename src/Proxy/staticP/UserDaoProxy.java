package Proxy.staticP;

/**
 * Created by Saul on 2019/6/18 16:20
 */
public class UserDaoProxy {
    /**
     *接收保存目标对象
     */
    private IUserDao target;
    public UserDaoProxy(IUserDao target){
        this.target=target;
    }

    public void save() {
        System.out.println("开始事务...");
        target.save();//执行目标对象的方法
        System.out.println("提交事务...");
    }

    public static void main(String[] args) {
        IUserDao iUserDao = new UserDao();
        UserDaoProxy userDaoProxy = new UserDaoProxy(iUserDao);
        userDaoProxy.save();
    }
}
